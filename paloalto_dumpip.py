#!/usr/bin/env python3

# parse_ipsec(): sometimes only interface was used there's no local-address otherwise child3 can be used

import http.client, ssl, re
import xml.etree.ElementTree as ET
from datetime import datetime

today = datetime.today().strftime("%Y-%m-%d ")
config = {'<panorama|palo ip>': '<key>',
          '<panorama|palo ip>': '<key>',
          '<panorama|palo ip>': '<key>',
          '<panorama|palo ip>': '<key>'}

def save(filename, s):
    with open(filename, 'wb') as f:
        f.write(s)

def name(x):
    return x.attrib["name"]

def call_api(host, req):
    context = ssl.SSLContext()
    context.check_hostname = False
  
    headers = {"Authorization": "Basic " + config[host]}
  
    conn = http.client.HTTPSConnection(host, context=context, source_address=(("<optional source ip>", 0)))
    conn.request("GET", req, headers=headers)
  
    r = conn.getresponse()
    if r.status==200:
        return r.read()

def parse_ipsec(s):
    child0 = ET.fromstring(s)
    result = []

    for child1 in child0.findall("./result/template/entry"):
        for child2 in child1.findall("./config/devices/entry/network/ike/gateway/entry"):
            #child3 = re.sub(r'/.*', '', child2.findall("./local-address/ip")[0].text)
            child4 = child2.findall("./peer-address/ip")[0].text
            result.append([child4, name(child1)+"/"+name(child2)])
    return result

def parse_ip(s):
    child0 = ET.fromstring(s)
    result = []
    dev_subint_ip_map = {}
    
    for child1 in child0.findall("./result/template/entry"):
        dev_subint_ip_map[name(child1)] = {}
        for child2 in child1.findall("./config/devices/entry/network/interface"):
            for child3 in child2.findall("./aggregate-ethernet/entry"):
                for child4 in child3.findall("./layer3/units/entry"):
                    dev_subint_ip_map[name(child1)][name(child4)] = [name(child5) for child5 in child4.findall("./ip/entry")]
                dev_subint_ip_map[name(child1)][name(child3)] = [name(child4) for child4 in child3.findall("./layer3/ip/entry")]
            for child3 in child2.findall("./ethernet/entry"):
                for child4 in child3.findall("./layer3/units/entry"):
                    dev_subint_ip_map[name(child1)][name(child4)] = [name(child5) for child5 in child4.findall("./ip/entry")]
                dev_subint_ip_map[name(child1)][name(child3)] = [name(child4) for child4 in child3.findall("./layer3/ip/entry")]
            for child3 in child2.findall("./tunnel/units/entry"):
                dev_subint_ip_map[name(child1)][name(child3)] = [name(child4) for child4 in child3.findall("./ip/entry")]
        for child2 in child1.findall("./config/devices/entry/vsys/entry"):
            for child3 in child2.findall("./zone/entry"):
                for child4 in child3.findall("./network/layer3/member"):
                    if child4.text in dev_subint_ip_map[name(child1)]:
                        for ipmask in dev_subint_ip_map[name(child1)][child4.text]:
                            ip = re.sub(r'/.*', '', ipmask)
                            intf = name(child1) + "/" + name(child2) + "/" + name(child3) + "/" + child4.text
                            result.append([ipmask, intf, ip])
    return result

def parse_devices(s):
    child0 = ET.fromstring(s)
    result = []
    for child1 in child0.findall("./result/devices/entry"):
        hostname = child1.findall("hostname")  [0].text
        ip       = child1.findall("ip-address")[0].text
        model    = child1.findall("model")     [0].text
        sw_ver   = child1.findall("sw-version")[0].text
        sn       = child1.findall("serial")    [0].text
        result.append([hostname, ip, model, sw_ver, sn])
    return result

if __name__ == '__main__':
    s0 = call_api("<panorama|palo ip>", "/api/?type=config&action=get&xpath=/config/devices/entry[@name='localhost.localdomain']/template")
    save(today + '<panorama|palo ip> template.xml', s0)
    s1 = call_api("<panorama|palo ip>", "/api/?type=config&action=get&xpath=/config/devices/entry[@name='localhost.localdomain']/template")
    save(today + '<panorama|palo ip> template.xml', s1)

    t0 = call_api("<panorama|palo ip>", "/api/?type=op&cmd=<show><devices><all/></devices></show>")
    save(today + '<panorama|palo ip> showdev.xml', t0)
    t1 = call_api("<panorama|palo ip>", "/api/?type=op&cmd=<show><devices><all/></devices></show>")
    save(today + '<panorama|palo ip> showdev.xml', t1)
    
    if s0 and s1 and t0 and t1:
        print("======================marker==============")
        r = parse_ip(s0) + parse_ip(s1) + parse_ip(s2) + parse_ip(s3)
        r = sorted(r, key=lambda x: x[1])
        max_len = max(map(lambda x: len(x[1]), r))
        max_len = 90
        for ipmask,intf,ip in r:
            intf = intf.replace(" ", "")
            print(f'{ip:15s} {intf:{max_len}s} #{ipmask:s}')

        r = parse_ipsec(s0) + parse_ipsec(s1) + parse_ipsec(s2) + parse_ipsec(s3)
        r = sorted(r, key=lambda x: x[1])
        for ip,desc in r:
            # sometimes object is used instead of plain IP address
            pattern = r'\b(?:\d{1,3}\.){3}\d{1,3}\b'
            match = re.search(pattern, ip)
            if match:
                ip = match.group()
            print(f'{ip:15s} {desc:{max_len}s} #IPSEC')

        r = parse_devices(t0) + parse_devices(t1) + parse_devices(t2) + parse_devices(t3)
        r = sorted(r, key=lambda x: x[0])
        for hostname,ip,model,sw_ver,sn in r:
            print(f'{ip:15s} {hostname:{max_len}s} #PALO')
