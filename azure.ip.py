#https://learn.microsoft.com/en-us/rest/api/resources/subscriptions/list?tabs=HTTP

#https://learn.microsoft.com/en-us/rest/api/virtualnetwork/virtual-networks/list-all?tabs=HTTP

#https://learn.microsoft.com/en-us/rest/api/firewall/azure-firewalls/list-all?tabs=HTTP

#https://learn.microsoft.com/en-us/rest/api/load-balancer/load-balancers/list-all?tabs=HTTP

#https://learn.microsoft.com/en-us/rest/api/virtual-network/
#https://learn.microsoft.com/en-us/rest/api/virtualnetwork/firewall-policies/list-all?tabs=HTTP

import http.client, json, time

config = {
        1: {'name': '<name>',
            'key': 'Bearer <bearer>'},
        2: {'name': '<name>',
            'key': 'Bearer <bearer>'},
	}

def extract_networks(config):
	dirname = config['name']
	headers = {"Authorization": config['key']}
	req1 = '/subscriptions?api-version=2020-01-01'
	conn = http.client.HTTPSConnection("management.azure.com")

	conn.request("GET", req1, headers=headers)
	r = conn.getresponse()
	if r.status==200:
		values1 = json.loads(r.read())['value']
		for value1 in values1:
			subid, subName = (value1['id'], value1['displayName'])
			req2 = f'/{subid}/providers/Microsoft.Network/virtualNetworks?api-version=2022-09-01'
			conn.request("GET", req2, headers=headers)
			r = conn.getresponse()
			if r.status==200:
				values2 = json.loads(r.read())['value']
				for value2 in values2:
					vnName = value2['name']
					for value22 in value2['properties']['subnets']:
						snName = value22['name']
						snPrefix = value22['properties']['addressPrefix']
						print(f'{snPrefix:18} | {dirname} | {subName} | {vnName} | {snName}')
						time.sleep(0.25)
	else:
		print(f'{dirname}: Update API key')

# TODO
def extract_(config):
	dirname = config['name']
	headers = {"Authorization": config['key']}
	req1 = '/subscriptions?api-version=2020-01-01'
	conn = http.client.HTTPSConnection("management.azure.com")

	conn.request("GET", req1, headers=headers)
	r = conn.getresponse()
	if r.status==200:
		values1 = json.loads(r.read())['value']
		for value1 in values1:
			subid, subName = (value1['id'], value1['displayName'])
			req2 = f'/{subid}/providers/Microsoft.Network/loadBalancers?api-version=2022-09-01'
			req2 = f'/{subid}/providers/Microsoft.Network/firewallPolicies?api-version=2022-09-01'
			conn.request("GET", req2, headers=headers)
			r = conn.getresponse()
			print(r.read())
	else:
		print(f'{dirname}: Update API key')

def test_key(config):
	dirname = config['name']
	headers = {"Authorization": config['key']}
	req1 = '/subscriptions?api-version=2020-01-01'
	conn = http.client.HTTPSConnection("management.azure.com")

	conn.request("GET", req1, headers=headers)
	r = conn.getresponse()
	if r.status==200:
		print(f'Good: {dirname}')
	else:
		print(f'Bad:  {dirname}')
		
#print(extract_(config05))

extract_networks(config[1])