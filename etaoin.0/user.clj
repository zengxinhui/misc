;; azure login
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(ns user
  (:require [etaoin.api :as e]
            [etaoin.keys :as k]))

(def driver (e/firefox))

(e/go driver "https://portal.azure.com")

(e/fill driver {:type :email} "xyz@msft.com")
(e/click driver {:type :submit})

(e/fill driver {:type :password} "xyz")
(e/click driver {:id :submitButton})

(e/click driver {:data-value :PhoneAppNotification})

(def saved-cookies (e/get-cookies driver))

(e/click driver {:type :checkbox})
(e/click driver {:type :submit})

;; preload cookies
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def driver1 (e/firefox))

(e/go driver1 "https://portal.azure.com")

(for [cookie saved-cookies]
  (e/set-cookie driver1 cookie))

(e/go driver1 "https://portal.azure.com")
