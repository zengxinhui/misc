import re
import urllib.request
from bs4 import BeautifulSoup

with urllib.request.urlopen('https://support.juniper.net/support/eol/product/srx_series/') as f:
    html_doc = f.read().decode('utf-8')

tbody = html_doc[html_doc.find("<tbody>") : html_doc.find("</tbody>")+len("</tbody>")]
soup = BeautifulSoup(tbody, 'html.parser')

result = []
for tr in soup.tbody.find_all('tr'):
    tds = tr.find_all('td')
    dates = [x.string if x.string and re.search(r'\d+/\d+/\d+', x.string) else None for x in tds[1:]]
    for product in re.findall(r"[\w-]+", tds[0].get_text()):
        result.append([product] + dates)

for r in result:
    print(r)
