(ns eol
  (:require [etaoin.api :as e]
            [etaoin.keys :as k]
            [clojure.string :as str]))

(def driver (e/firefox {:headless true}))

(e/go driver "https://support.juniper.net/support/eol/product/srx_series/")
(e/wait-visible driver [{:tag :sw-eol-table}])

;; Table Columns:
;;   1. Product/SKU
;;   2. EOL Announced
;;   3. Last Order
;;   4. Last Date to Convert Warranty
;;   5. Same Day Support Discouninued
;;   6. Next Day Support Discontinued
;;   7. End of Support

;; Returns a list of [1 2 3 4 5 6 7]
;; Any date not in the form of xx/xx/xxxx is replaced with 'nil'.
;; E.g: 
;;     (["SRX-CFP-100G-SR10"
;;       ["04/15/2023"
;;        "04/15/2023"
;;        nil
;;        "04/15/2026"
;;        "04/15/2026"
;;        "04/15/2026"]]
;;      ["SRX600-PWR-645DC-POE"
;;       ["09/15/2022"
;;        "03/15/2023"
;;        "03/15/2024"
;;        "03/15/2028"
;;        "03/15/2028"
;;        "03/15/2028"]] ...)

(let [table (e/query driver {:css "sw-eol-table tbody"})]
  (for [row (e/children driver table {:tag :tr})
        :let [[product-td & more-tds] (e/children driver row {:tag :td})
              dates (->> more-tds
                         (mapv #(let [date (e/get-element-text-el driver %)]
                                  (if (re-matches #"\d+/\d+/\d+" date) date))))]
        ;; Use regex to split products/SKU field "xx, yy, zz"
        product (re-seq #"[\w-]+" (e/get-element-text-el driver product-td))]
    (cons product dates)))
