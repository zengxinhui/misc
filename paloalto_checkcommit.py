#!/usr/bin/env python3
import http.client, ssl, re
import xml.etree.ElementTree as ET

config = {'key1': 'key1',
          'key2': 'key2',
          'key3': 'key3'}

prog = re.compile("<username1|username2|...>")

def call_api(req):
  context = ssl.SSLContext()
  context.check_hostname = False
  
  headers = {"X-PAN-KEY": config['key2']}
  
  conn = http.client.HTTPSConnection("<panorama|palo ip>", context=context, source_address=(("<optional source ip", 0)))
  conn.request("GET", req, headers=headers)
  
  r = conn.getresponse()
  if r.status==200:
    return r.read()

def main(s):
  entries = ET.fromstring(s).findall("./result/entry")
  entries.reverse()
  for entry in entries:
    commit_no = entry.attrib["name"]
    timestamp = entry.find("timestamp").text
    admin     = entry.find("admin").text
    desc      = entry.find("description").text
    if prog.search(admin):
        print("{} = {} = {} = {}\n".format(commit_no, timestamp, admin, desc))

if __name__ == '__main__':
  s = call_api("/api/?type=op&cmd=<show><config><audit><info></info></audit></config></show>")
  if s:
    main(s)
