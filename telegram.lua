require 'luci.sys'
require 'luci.jsonc'

while true do
  url = "https://api.telegram.org/bot<token>/getUpdates?timeout=28"
  if offset then
    url = string.format("%s&offset=%d", url, offset+1)
  end
  print(url)
  
  data0=luci.sys.httpget(url)
  data1=luci.jsonc.parse(data0) 
  
  print(data0)
  print(data1)
  
  if data1 and data1.ok==true then
    for i,v in ipairs(data1.result) do
      if not offset or offset<v.update_id then
          offset = v.update_id
      end
      if v.message and v.message.text == "/larryoff" then
        luci.sys.exec('ip ro add blackhole 192.168.1.32/29')
      elseif v.message and v.message.text == "/larryon" then
        luci.sys.exec('ip ro del 192.168.1.32/29')
      end
    end
  end
end
