socket = require("socket")

function handle_request(client)
  local line, err = client:receive()
  if err then
    print("Error reading request:", err)
    return
  end
  
  local method, path, http_version = string.match(line, "^(%S+)%s+(%S+)%s+(%S+)$")
  if not method or not path or not http_version then
    print("Invalid request line:", line)
    return
  end
  print("Received request:", method, path, http_version)

  local route, t = string.match(path, "(/%S+)%?time=(.*)")
  if route == "/ban" then
    os.execute("(ip ro del blackhole 192.168.1.32/29; ip ro del blackhole 192.168.1.81/32; sleep " .. t .. "; ip ro add blackhole 192.168.1.32/29; ip ro add blackhole 192.168.1.81/32) &")
  elseif route == "/ban0" then
    os.execute("ip ro add blackhole 192.168.1.90/32")
  elseif route == "/ban1" then
    os.execute("ip ro del blackhole 192.168.1.90/32; ip ro del blackhole 192.168.1.32/29; ip ro del blackhole 192.168.1.81/32")
  end
  
  -- Send a simple response
  local response = [[<!DOCTYPE html>
<html>
  <head>
    <title>OpenWrt</title>
    <meta name="viewport" content="width=device-width, initial-scale=2">
  </head>
  <body>
    <form action="/ban" method="get">
      <label>Time:</label>
      <input type="text" name="time"><br>
      <input type="submit" value="Submit">
    </form>
    <form action="/ban0" method="get">
      <input type="hidden" name="time" value="-1">
      <input type="submit" value="ban li">
    </form>
    <form action="/ban1" method="get">
      <input type="hidden" name="time" value="-1">
      <input type="submit" value="unban all">
    </form>
  </body>
</html>]]
  local response = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: " .. #response .. "\r\n\r\n" .. response
  client:send(response)
  client:close()
end

function run_server(port)
  local server = assert(socket.bind("*", port))
  print("Server running on port", port)
  while true do
    local client = server:accept()
    print("New client connected:", client:getpeername())
    handle_request(client)
  end
end

run_server(9090)
