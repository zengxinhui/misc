;; https://svg-tutorial.com/

(ns app.hello
  (:require [reagent.core :as r]))

(defonce timer (r/atom (js/Date.)))

(defonce timer-updater (js/setInterval #(reset! timer (js/Date.)) 1000))

(defn day22 []
  ;; analog clock
  (fn []
    (let [minutes        (.getMinutes @timer)
          hours-degree   (+ (* (/ 360.0 12) (mod (.getHours @timer) 12))
                            (* minutes 0.5))
          minutes-degree (* (/ 360 60) (.getMinutes @timer))]
      [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
       [:rect {:x -100 :y -100 :width 200 :height 200 :fill "#CD803D"}]
       [:circle {:r 55 :stroke "#FCCE7B" :stroke-width 10 :fill "white"}]
       [:circle {:r 45 :stroke "#B6705F" :stroke-width 6  :fill "none"
                 :stroke-dasharray "6 17.56194490192345" :stroke-dashoffset 3}]
       [:g {:stroke "#5f4c6c" :stroke-linecap "round"}
        [:line#hours   {:y2 -20 :stroke-width 8 :transform (str "rotate(" hours-degree   ")")}]
        [:line#minutes {:y2 -35 :stroke-width 6 :transform (str "rotate(" minutes-degree ")")}]]])))

(defn day23 []
  ;; colorful lights than can be turned on or off
  (let [lightsOn (r/atom nil)]
    (fn []
      [:svg.lights {:width 400 :height 400 :viewBox "-200 -200 400 400"}
       [:defs
        [:g {:id "bulb"}
         [:path {:d "M 0,0 Q 20 25 0 40 Q -20 25 0 0"}]
         [:rect {:x -6 :y -1 :width 12 :height 10 :rx 3 :fill "#5F4C6C"}]]]

       [:path {:d "M -140 -60 Q -70 -50 0 -60 Q 110 -70 110 10"}]
       [:line {:x1 -70 :y1 -15 :x2 -70 :y2 -55}]
       [:line {:x1  30 :y1 -25 :x2  30 :y2 -60}]
       [:use.b {:href "#bulb" :fill (if @lightsOn "#FFC05B" "white") :x -120 :y -45 :transform "rotate(5)" }]
       [:use.b {:href "#bulb" :fill (if @lightsOn "#F86285" "white") :x  -70 :y -15}]
       [:use.b {:href "#bulb" :fill (if @lightsOn "#03A8A8" "white") :x  -20 :y -57 :transform "rotate(-5)" }]
       [:use.b {:href "#bulb" :fill (if @lightsOn "#748CEF" "white") :x   30 :y -25}]

       [:rect {:x 90 :y 10 :width 40 :height 40 :fill "lightgray"}]

       [:circle#button {:cx 110 :cy 30 :r 15 :fill "red" :onClick #(swap! lightsOn not)}]])))

(defn hello []
  [:<>
   ;; Day 1
   [:svg {:width 100 :height 100 :viewBox "0 0 100 100"}
    [:circle {:cx 50 :cy 50 :r 25 :fill "red"}]]

   [:svg {:width 100 :height 100 :viewBox "0 0 200 200"}
    [:circle {:cx 100 :cy 100 :r 50}]]
   [:svg {:width 200 :height 200 :viewBox "0 0 200 200"}
    [:circle {:cx 100 :cy 100 :r 50}]]
   [:svg {:width 200 :height 200 :viewBox "0 0 100 100"}
    [:circle {:cx 100 :cy 100 :r 50}]]

   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:circle {:cx 0 :cy 0 :r 50}]]

   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:circle {:cx 0 :cy 20 :r 70 :fill "#D1495B"}]
    [:rect {:x -17.5 :y -65 :width 35 :height 20 :fill "#F79257"}]
    [:circle {:cx 0 :cy -75 :r 12 :fill "none" :stroke "#F79257" :stroke-width 2}]]

   ;; Day 2
   [:svg {:width 200 :height 400 :viewBox "-100 -200 200 400"}
    [:polygon {:points "0,0   80,120 -80,120" :fill "#234236"}]
    [:polygon {:points "0,-40 60,60  -60,60"  :fill "#0c5c4c"}]
    [:polygon {:points "0,-80 40,0   -40,0"   :fill "#38755b"}]
    [:rect {:x -20 :y 120 :width 40 :height 30 :fill "brown"}]]

   ;; Day 3
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:circle.head {:cx 0 :cy -50 :r 30}]

    [:circle.eye {:cx -12 :cy -55 :r 3}]
    [:circle.eye {:cx  12 :cy -55 :r 3}]
    [:rect.mouth {:x -10 :y -40 :width 20 :height 5 :rx 2}]

    [:line.limb {:x1 -40 :y1 -10 :x2 40 :y2 -10}]
    [:line.limb {:x1 -25 :y1  50 :x2  0 :y2 -15}]
    [:line.limb {:x1  25 :y1  50 :x2  0 :y2 -15}]

    [:circle.button {:cx 0 :cy -10 :r 5}]
    [:circle.button {:cx 0 :cy  10 :r 5}]]

   ;; Day 4
   [:svg.house {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:polygon.wall {:points "-65,80 -65,-10 0,-70 65,-10 65,80"}]
    [:polyline.roof {:points "-75,-8 0,-78 75,-8"}]

    [:rect.door {:x -45 :y 10 :width 30 :height 60 :rx 2}]
    [:circle.door-knob {:cx -35 :cy 40 :r 2}]
    [:rect.stair {:x -47 :y 70 :width 34 :height 5}]
    [:rect.stair {:x -49 :y 75 :width 38 :height 5}]

    [:rect.window {:x 5 :y 15 :width 40 :height 35 :rx 5}]
    [:line {:x1 5  :y1 32.5 :x2 45 :y2 32.5}]
    [:line {:x1 25 :y1 15   :x2 25 :y2 50}]
    [:rect.window-still {:x 2 :y 48 :width 46 :height 5 :rx 5}]

    [:circle.window {:cx 0 :cy -25 :r 15}]
    [:line {:x1 -15 :y1 -25 :x2 15 :y2 -25}]
    [:line {:x1   0 :y1 -40 :x2  0 :y2 -10}]]

   ;; Day 5
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:path {:stroke "#333333" :stroke-width 25 :stroke-linecap "round"
            :d "M -40,-40 L 40,-40 M -40,0 L 40,0 M -40,40 L 40,40"}]
    [:path {:fill "none"
            :transform "translate(0 -80) scale(0.2 0.2)" ;; workaround
            :stroke "black" :stroke-width 80 :stroke-linecap "round" :stroke-linejoin "round"
            :d "M -30,-20 L 0,10 L 30,-20"}]
    [:path {:fill "none"
            :transform "translate(0  80) scale(0.2 0.2)" ;; workaround
            :stroke "#d1495b" :stroke-width 25 :stroke-linecap "round" :stroke-linejoin "round"
            :d "M -70,0 L 70,0 L 30,-50 M 70,0 L 30,50"}]]

   ;; Day 6
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:g {:transform "translate(0 5)"}
     [:g
      [:polygon {:points "0,0  36,-50 0,-100" :fill "#EDD8B7"}]
      [:polygon {:points "0,0 -36,-50 0,-100" :fill "#E5C39C"}]]
     [:g {:transform "rotate(72)"}
      [:polygon {:points "0,0  36,-50 0,-100" :fill "#EDD8B7"}]
      [:polygon {:points "0,0 -36,-50 0,-100" :fill "#E5C39C"}]]
     [:g {:transform "rotate(-72)"}
      [:polygon {:points "0,0  36,-50 0,-100" :fill "#EDD8B7"}]
      [:polygon {:points "0,0 -36,-50 0,-100" :fill "#E5C39C"}]]
     [:g {:transform "rotate(144)"}
      [:polygon {:points "0,0  36,-50 0,-100" :fill "#EDD8B7"}]
      [:polygon {:points "0,0 -36,-50 0,-100" :fill "#E5C39C"}]]
     [:g {:transform "rotate(-144)"}
      [:polygon {:points "0,0  36,-50 0,-100" :fill "#EDD8B7"}]
      [:polygon {:points "0,0 -36,-50 0,-100" :fill "#E5C39C"}]]]]

   ;; Day 7
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:defs
     [:path#branch {:stroke "#e5c39c" :stroke-width 5
                    :d "M 0   0 L   0 -90
                        M 0 -20 L  20 -34
                        M 0 -20 L -20 -34
                        M 0 -40 L  20 -54
                        M 0 -40 L -20 -54
                        M 0 -60 L  20 -74
                        M 0 -60 L -20 -74"}]]
    [:use {:href "#branch"}]
    [:use {:href "#branch" :transform "rotate(60)"}]
    [:use {:href "#branch" :transform "rotate(120)"}]
    [:use {:href "#branch" :transform "rotate(180)"}]
    [:use {:href "#branch" :transform "rotate(240)"}]
    [:use {:href "#branch" :transform "rotate(300)"}]]

   ;; Day 8
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:defs
     [:g#tree
      [:polygon {:points "-10,0 10,0 0,-50" :fill "#38766b"}]
      [:line {:x1 0 :y1 0 :x2 0 :y2 10 :stroke "#778074" :stroke-width 2}]]]

    [:rect {:x -100 :y -100 :width 200 :height 200 :fill "#F1DBC3"}]
    [:circle {:cx 0 :cy 380 :r 350 :fill "#F8F4E8"}]

    [:use {:href "#tree" :x -30 :y 25 :transform "scale(2)"}]
    [:use {:href "#tree" :x -20 :y 40 :transform "scale(1.2)"}]
    [:use {:href "#tree" :x  40 :y 40}]
    [:use {:href "#tree" :x  50 :y 30 :transform "scale(1.5)"}]]

   ;; Day 9
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:defs
     [:clipPath#ball
      [:circle {:cx 0 :cy 20 :r 70}]]]

    [:circle {:cx 0 :cy 20 :r 70 :fill "#D1495B"}]

    [:polyline {:clip-path "url(#ball)" :fill "none" :stroke "#9c2d2a" :stroke-width 20
                :points "-120 40 -80 0 -40 40 0 0 40 40 80 0 120 40"}]

    [:circle {:cx 0 :cy -75 :r 12 :fill "none" :stroke "#F79257" :stroke-width 2}]
    [:rect {:x -17.5 :y -65 :width 35 :height 20 :fill "#F79257"}]]

   ;; Day 10
   [:svg {:width 200 :height 400 :viewBox "-100 -200 200 400" :style {:background-color "lightblue"}}
    [:defs
     [:radialGradient#snowball {:cx 0.25 :cy 0.25 :r 1}
      [:stop {:offset   "0%" :stop-color "white"}]
      [:stop {:offset  "50%" :stop-color "white"}]
      [:stop {:offset "100%" :stop-color "#d6d6d6"}]]]

    [:circle {:cx 0 :cy  60 :r 80 :fill "url(#snowball)"}]
    [:circle {:cx 0 :cy -40 :r 50 :fill "url(#snowball)"}]
    [:polygon {:points "10,-46 50,-40 10,-34" :fill "#e66465"}]

    [:circle {:cx  0 :cy -55 :r 5}]
    [:circle {:cx 20 :cy -55 :r 5}]

    [:line {:x1 -40 :y1 30 :x2 -90 :y2 -30 :stroke "black" :stroke-width 5}]
    [:line {:x1 -65 :y1  0 :x2 -90 :y2 -10 :stroke "black" :stroke-width 5}]]

   ;; Day 11
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:circle {:cx -50 :cy -50 :r 20}]
    [:circle {:cx  50 :cy -50 :r 20}]
    [:path {:fill "none" :stroke-width 10 :stroke-linecap "round" :stroke "black"
            :d "M -70,50 Q 0,100 70,50"}]]

   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:path {:fill "none" :stroke-width 5 :stroke "black"
            :d "M -50,50 Q 50,-50 50,50"}]
    [:circle {:cx 50 :cy -50 :r 5}]]

   [:svg {:width 200 :height 400 :viewBox "-100 -200 200 400"}
    [:path {:fill "none" :stroke-width 5 :stroke "#0c5c4c"
            :d "         M 0 -80
                Q   5  -75 0 -70
                Q -10  -65 0 -60
                Q  15  -55 0 -50
                Q -20  -45 0 -40
                Q  25  -35 0 -30
                Q -30  -25 0 -20
                Q  35  -15 0 -10
                Q -40   -5 0   0
                Q  45    5 0  10
                Q -50   15 0  20
                Q  55   25 0  30
                Q -60   35 0  40
                Q  65   45 0  50
                Q -70   55 0  60
                Q  75   65 0  70
                Q -80   75 0  80
                Q  85   85 0  90
                Q -90   95 0 100
                Q  95  105 0 110
                Q -100 115 0 120
                L   0 140
                L  20 140
                L -20 140"}]]

   [:svg {:width 200 :height 400 :viewBox "-100 -200 200 400"}
    [:path {:fill "none" :stroke "black"
            :d "           M 0 -80
                L   5  -75 L 0 -70
                L -10  -65 L 0 -60
                L  15  -55 L 0 -50
                L -20  -45 L 0 -40
                L  25  -35 L 0 -30
                L -30  -25 L 0 -20
                L  35  -15 L 0 -10
                L -40   -5 L 0   0
                L  45    5 L 0  10
                L -50   15 L 0  20
                L  55   25 L 0  30
                L -60   35 L 0  40
                L  65   45 L 0  50
                L -70   55 L 0  60
                L  75   65 L 0  70
                L -80   75 L 0  80
                L  85   85 L 0  90
                L -90   95 L 0 100
                L  95  105 L 0 110
                L -100 115 L 0 120
                L   0 140
                L  20 140
                L -20 140"}]]

   ;; Day 12
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:path {:fill "none" :stroke "red"
            :d "M -50,50 C -60,-50 60,-50 50 50"}]
    [:circle {:cx -60 :cy -50 :r 2}]
    [:circle {:cx  60 :cy -50 :r 2}]]

   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:path {:fill "none" :stroke "red"
            :d "M  0 0
                L 30 0
                C 50 0 50 -25 30 -15
                L  0 0"}]
    [:circle {:cx 50 :cy   0 :r 2}]
    [:circle {:cx 50 :cy -20 :r 2}]]

   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:circle {:cx 0 :cy -50 :r 10 :fill "#a9172a"}]
    [:rect.box {:x -60 :y -40 :width 120 :height 100}]
    [:rect.box {:x -70 :y -47 :width 140 :height 20}]
    [:rect.stripe {:x -20 :y -40 :width 40 :height 100}]
    [:rect.stripe {:x -25 :y -47 :width 50 :height 20}]
    [:path.ribbon {:d "M  0 -50
                       L 30 -50
                       C 50 -50 50 -70 30 -65
                       L  0 -50"}]
    [:path.ribbon {:d "M   0 -50
                       L -30 -50
                       C -50 -50 -50 -70 -30 -65
                       L   0 -50"}]]

   ;; Day 13
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200" :stroke "black" :stroke-width 2}
    [:circle {:cx 0 :cy -45 :r  7 :fill "#4f6d7a"}]
    [:circle {:cx 0 :cy  50 :r 10 :fill "#f79257"}]
    [:path {:fill "#fdea96"
            :d "M -50  40
                L -50  50
                L  50  50
                L  50  40
                Q  40  40  40  10
                C  40 -60 -40 -60 -40 10
                Q -40  40 -50  40"}]]

   ;; Day 14
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200" :stroke "black" :stroke-width 2}
    [:path {:fill "none" :stroke "red" :stroke-width 2
            :d "M -40 -40
                A  70  40 30 1 1 40 40"}]]

   [:svg {:width 200 :height 400 :viewBox "-100 -200 200 400"}
    [:path.body {:stroke "#cd803d" :stroke-width 45
                 :d "M 50 120
                     L 50 -80
                     A 50  50 0 0 0 -50 -80"}]
    [:path.body {:stroke "white" :stroke-width 40
                 :d "M 50 120
                     L 50 -80
                     A 50  50 0 0 0 -50 -80"}]
    [:line.green-mark  {:x1 -35 :y1 -90  :x2 -60 :y2 -100}]
    [:line.red-mark    {:x1 -15 :y1 -115 :x2 -25 :y2 -135}]
    [:line.green-mark  {:x1  20 :y1 -110 :x2  35 :y2 -130}]
    [:line.red-mark    {:x1  40 :y1 -60  :x2  60 :y2 -80}]
    [:line.green-mark  {:x1  40 :y1 -10  :x2  60 :y2 -30}]
    [:line.red-mark    {:x1  40 :y1  40  :x2  60 :y2  20}]
    [:line.green-mark  {:x1  40 :y1  90  :x2  60 :y2  70}]]

   ;; Day 15
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:defs
     [:path#ribbon {:fill "#b73a3b"
                    :d "M  0 -20
                        Q 28 -40 56 -45
                        C 96 -48 96  48 56 45
                        Q 28  40  0  20"}]]
    [:use {:href "#ribbon"}]
    [:use {:href "#ribbon" :transform "scale(-1)"}]
    [:ellipse {:cx 0 :cy 0 :rx 20 :ry 24 :fill "#9c2d2a"}]
    [:path {:fill "none" :stroke-width 3 :stroke "#b73a3b"
            :d "M  0 20
                Q 40 40 30 60
                Q 20 80 40 90

                M   0 20
                Q -30 30 -20 60
                Q -10 90 -50 100"}]]

   ;; Day 16
   [:svg.bear {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:circle.ear {:cx -40 :cy -50 :r 18}]
    [:circle.ear {:cx  40 :cy -50 :r 18}]
    [:rect.face {:x -55 :y -60 :width 110 :height 120 :rx 50 :ry 30}]
    [:circle {:cx  20 :cy -30 :r 3}]
    [:circle {:cx -20 :cy -30 :r 3}]
    [:path {:fill "#e5c39c"
            :d "M -30   0
                C -30 -25  30 -25 30 0
                L  30  30
                Q  30  40  20 40
                L -20  40
                Q -30  40 -30 30"}]
    [:path {:d "M -10 0
                L  10 0
                C 10 20 -10 20 -10 0"}]
    [:path.mouth {:d "M 0 10
                      Q 0 25  10 25
                      M 0 10
                      Q 0 25 -10 25"}]]

   ;; Day 17
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:defs
     [:path#text-arc {:d "M 0, 50 A 50 50 0 1 1 1,50"}]]
    [:text {:fill "#0c5c4c" :font-family "Tahoma" :font-size "0.77em" :font-weight "bold"}
     [:textPath {:href "#text-arc"} "Happy Hoildays! Happy Hoildays! Happy Hoildays!"]]]

   ;; Day 18
   [:svg {:width 400 :height 200 :viewBox "-200 -100 400 200" :fill "none"}
    [:path {:stroke "#e0ceb9" :stroke-width 4
            :d "M-200 80 L -80 80 Q 80 80 70 -10 A 70 70 0 0 0 -70 -10 Q -80 80 80 80 L 200 80"}]
    [:g.sleigh
     [:path {:stroke-width 5 :stroke "#af6455"
             :d "M -30 -2 L  30 -2  A 10 10 0 0 0 30 -22
                 M -20 -2 L -20 -17
                 M  20 -2 L  20 -17"}]
     [:path {:stroke-width 6 :stroke "#7a504f"
             :d "M -27 -17 L 27 -17"}]]]

   ;; Day 19
   [:svg.bell {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:g {:stroke "#001514" :stroke-width 2}
     [:circle {:cx 0 :cy -45 :r  7 :fill "#4f6d7a"}]
     [:circle.bell-tongue {:cx 0 :cy  50 :r 10 :fill "#f79257"}]
     [:path {:fill "#fdea96"
             :d "M -50  40
                 L -50  50
                 L  50  50
                 L  50  40
                 Q  40  40  40  10
                 C  40 -60 -40 -60 -40 10
                 Q -40  40 -50  40"}]]]

   ;; Day 20
   [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
    [:defs
     [:g#tree
      [:polygon {:points "-10,0 10,0 0,-50" :fill "#38755b"}]
      [:line {:x2 0 :y2 10 :stroke "#778074" :stroke-width 2}]]
     [:circle#big   {:cx 0 :cy 0 :r 5 :fill "white"}]
     [:circle#small {:cx 0 :cy 0 :r 3 :fill "white"}]]

    [:rect {:x -100 :y -100 :width 200 :height 200 :fill "#F1DBC3"}]
    [:circle {:cx 0 :cy 380 :r 350 :fill "#F8F4E8"}]

    [:use {:href "#tree" :x -30 :y 25 :transform "scale(2)"}]
    [:use {:href "#tree" :x -20 :y 40 :transform "scale(1.2)"}]
    [:use {:href "#tree" :x  40 :y 40}]
    [:use {:href "#tree" :x  50 :y 30 :transform "scale(1.5)"}]

    [:use.flake.fast        {:href "#big"   :x   0 :y   0}]
    [:use.flake.fast.opaque {:href "#big"   :x -50 :y -20}]
    [:use.flake.fast        {:href "#big"   :x  30 :y -40}]
    [:use.flake.fast.opaque {:href "#big"   :x  50 :y -20}]
    [:use.flake.slow        {:href "#big"   :x  30 :y  50}]
    [:use.flake.slow.opaque {:href "#big"   :x -70 :y -80}]
    [:use.flake.slow        {:href "#big"   :x  30 :y  50}]
    [:use.flake.slow.opaque {:href "#big"   :x  90 :y -80}]
    [:use.flake.slow        {:href "#small" :x  10 :y -50}]
    [:use.flake.slow.opaque {:href "#small" :x -50 :y -60}]
    [:use.flake.slow        {:href "#small" :x  30 :y  70}]
    [:use.flake.slow.opaque {:href "#small" :x  10 :y -80}]]

   ;; Day 21
   [:a.close {:href "/"}]

   [:svg {:width 50 :height 50 :viewBox "0 0 100 100"}
    [:path {:stroke-width 10 :stroke "black"
            :d "M 30 30
                L 70 70
                M 30 70
                L 70 30"}]]

   [:svg {:width 50 :height 50 :viewBox "0 0 120 120"}
    [:polyline {:fill "none" :stroke "#0c5c4c" :stroke-width 42.4 :points "-30 0 60 90 150 0"}]]

   [:div.background]

   ;; Day 22
   [day22]

   ;; Day 23
   [day23]

   ;; Day 24
   (let [dataPoints [3 4 7 5 3 6]
         sineWave (->> (range 115)
                       (map #(str (- % 55) "," (+ (* (Math/sin(/ % 20)) 20) 10)))
                       (clojure.string/join " "))
         dp-fn (fn [index dp]
                 [:rect {:key index, :x (- (* index 20) 55), :y (- 50 (* dp 10))
                         :width 15, :height (* 10 dp) :fill "#CD803D"}])]
     [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
      (map-indexed dp-fn dataPoints)
      [:polyline {:points sineWave :fill "none" :stroke "black" :stroke-width 5}]])

   ;; Day 25
   (let [trees [:g
                 [:defs
                  [:g#tree
                   [:polygon {:points "-10,0 10,0 0,-50" :fill "#38755b"}]
                   [:line {:x2 0 :y2 10 :stroke "#778074" :stroke-width 2}]]]
                 [:use {:href "#tree" :x -20 :y 25 :transform "scale(1.8)"}]
                 [:use {:href "#tree" :x -10 :y 40 :transform "scale(1)"}]
                 [:use {:href "#tree" :x  30 :y 40 :transform "scale(0.8)"}]
                 [:use {:href "#tree" :x  40 :y 30 :transform "scale(1.2)"}]]
         snow [:g.snowing
               [:defs
                [:circle#big   {:cx 0 :cy 0 :r 5 :fill "white"}]
                [:circle#small {:cx 0 :cy 0 :r 3 :fill "white"}]]
               [:use.flake.fast        {:href "#big"   :x   0 :y   0}]
               [:use.flake.fast.opaque {:href "#big"   :x -50 :y -20}]
               [:use.flake.fast        {:href "#big"   :x  30 :y -40}]
               [:use.flake.fast.opaque {:href "#big"   :x  50 :y -20}]
               [:use.flake.slow        {:href "#big"   :x  30 :y  50}]
               [:use.flake.slow.opaque {:href "#big"   :x -70 :y -80}]
               [:use.flake.slow        {:href "#big"   :x  30 :y  50}]
               [:use.flake.slow.opaque {:href "#big"   :x  90 :y -80}]
               [:use.flake.slow        {:href "#small" :x  10 :y -50}]
               [:use.flake.slow.opaque {:href "#small" :x -50 :y -60}]
               [:use.flake.slow        {:href "#small" :x  30 :y  70}]
               [:use.flake.slow.opaque {:href "#small" :x  10 :y -80}]]
         snowGlobe [:svg {:width 200 :height 200 :viewBox "-100 -100 200 200"}
                    [:clipPath#snow-globe
                     [:circle {:cx 0 :cy 0 :r 80}]]
                    [:g {:clip-path "url(#snow-globe)"}
                     [:rect {:x -100 :y -100 :width 200 :height 200 :fill "#f1dbc3"}]
                     [:circle {:cx 0 :cy 380 :r 350 :fill "#F8F4E8"}]
                     trees
                     snow]
                    [:circle {:cx 0 :cy 0 :r 80 :fill "none" :stroke "gray" :stroke-width 2}]]]
     snowGlobe)])
