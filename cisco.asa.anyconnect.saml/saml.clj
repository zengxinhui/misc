(ns saml
  (:require [clojure.xml        :as xml]
            [etaoin.api         :as e]
            [etaoin.keys        :as k]
            [org.httpkit.client :as http]))

(defonce config
  (-> (slurp "config.edn") (clojure.edn/read-string)))

(defonce template1 "<?xml version='1.0' encoding='UTF-8'?><config-auth client=\"vpn\" type=\"init\" aggregate-auth-version=\"2\"><version who=\"vpn\">4.7.00136</version><device-id>linux-64</device-id><group-select>%s</group-select><capabilities><auth-method>single-sign-on-v2</auth-method></capabilities></config-auth>")

(defonce template2 "<?xml version='1.0' encoding='UTF-8'?><config-auth client=\"vpn\" type=\"auth-reply\" aggregate-auth-version=\"2\"><version who=\"vpn\">4.7.00136</version><device-id>linux-64</device-id><session-token/><session-id/>%s%s</config-auth>")

(defonce template3 "sudo openconnect -b -i %s -s %s -C %s %s")

(defonce http-options {:url        (:server config)
                       :method     :post
                       :headers    {"Accept"              "*/*"
                                    "Accept-Encoding"     "identity"
                                    "X-Transcend-Version" "1"
                                    "X-Aggregate-Auth"    "1"
                                    "X-Support-HTTP-Auth" "true"}
                       :user-agent "AnyConnect Linux_64 4.7.00136"
                       :body       :tbd})

(defonce driver (e/firefox))

(defn http-req-by-xml [body]
  (->> (:body @(http/request (assoc http-options :body body)))
       (.getBytes)
       (java.io.ByteArrayInputStream.)
       (xml/parse)))

(defn extract-login-url [xml]
  (->> (:content xml)
       (some #(if (= :auth         (:tag %)) %))
       :content
       (some #(if (= :sso-v2-login (:tag %)) %))
       :content
       first))

(defn get-saml-token [url]
  (e/go driver url)

  (e/wait-visible driver [{:tag :input :type :email}])
  (e/fill-human   driver  {:tag :input :type :email} (:email config))
  (e/fill         driver  {:tag :input :type :email} k/enter)

  (e/wait-visible driver [{:tag :input :type :password}])
  (e/fill-human   driver  {:tag :input :type :password} (:pass config))
  (e/fill         driver  {:tag :input :type :password} k/enter)

  (e/wait-visible driver [{:tag :input :type :submit}] {:timeout 120})
  (e/wait         driver 9)
  (e/click        driver [{:tag :input :type :submit}])

  (e/wait-visible driver {:class :log-success})
  (:value (e/get-cookie driver "acSamlv2Token")))

(defn get-cookie-map [req]
  ;; asa session-token == openconnect cookie
  (reduce (fn [r n]
            (let [xml2 (http-req-by-xml req)]
              (if-let [session-token (->> (:content xml2)
                                          (some #(if (= :session-token (:tag %)) %)))]
                (-> (assoc r :cookie (first (:content session-token)))
                    (update :xml2 conj xml2)
                    (reduced))
                (do (when (< n 2)
                      (println "Try ag after 10sec")
                      (Thread/sleep 10000))
                    (update r :xml2 conj xml2)))))
          {:xml2 []}
          (range 3)))

(let [req1 (format template1 (:group config))
      xml1 (http-req-by-xml req1)
      url (extract-login-url xml1)
      token (get-saml-token url)
      req2 (-> (format template2
                       (with-out-str
                         (xml/emit-element (some #(if (= :opaque (:tag %)) %) (:content xml1))))
                       (with-out-str
                         (xml/emit-element {:tag :auth :content [{:tag :sso-token :content [token]}]})))
               (clojure.string/replace "\n" ""))
      m (get-cookie-map req2)
      r (if (:cookie m)
          (format template3 (:intf config) (:script config) (:cookie m) (:server config))
          (merge m {:req1 req1, :xml1 xml1, :url url, :token token, :req2 req2}))]
  (prn r))
